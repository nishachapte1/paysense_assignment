import React from 'react';
import { Link } from 'react-router-dom';

class FourZeroFourContainer extends React.Component {

    render() {
        const { user, users } = this.props;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h1> Sorry Page Not Found</h1>
                
                <p>
                    <Link to="/login">Logout</Link>
                </p>
            </div>
        );
    }
}

export default FourZeroFourContainer;