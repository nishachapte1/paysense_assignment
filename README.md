**# PaySense_assignment**


Steps to run the Project: 
1. npm i
2. npm start 


Pages created in this project:
1. Login Page
2. Register Page
3. Home Page
4. Page not found Page


**Note:Please do register before login **

Folder Structure:

1)React Components Folder
	Path: /src/_components
	The _components folder contains shared React components that can be used anywhere in the application.

2)Redux Action Constants Folder
	Path: /src/_constants
	The _constants folder contains all of the redux action type constants used by redux action creators and reducers
	
3)Helpers Folder
	Path: /src/_helpers
	The helpers folder contains all the bits and pieces that don't fit into other folders like auth and mock backend.
	
4)Redux Store
	Path: /src/_helpers/store.js	
	It contains initialization of thunk(use for api) and logger middleware(you can check all redux value in console once the project run.
	It will show you all state of redux value.)
	
5)Redux Reducers Folder
	Path: /src/_reducers
	The _reducers folder contains all the Redux reducers for the project, each reducer updates a different part of the application state in response to dispatched redux actions.
	
6)Services folder
	Path: src/_services
	The _services layer handles all http communication with backend apis for the application, each service encapsulates the api calls for a content type (e.g. users) and exposes methods for performing various operations (e.g. CRUD operations). 
	Services can also have methods that don't wrap http calls, for example the userService.logout() method just removes an item from local storage.
	
7)Path: /src/index.jsx
	The root index.jsx file bootstraps the application by rendering the App component (wrapped in a redux Provider) into the app div element defined in the base index html file above.
	The boilerplate application uses a mock backend that stores data in browser local storage, 
	to switch to a real backend api simply remove the fake backend code below the comment